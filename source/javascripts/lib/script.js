/* Author: jomko */

;(function() {
	
	$("#import").on("click", function(){
		$(this).hide();
		$("#form").show();
	})

	$(document).on("click", "#submit", function(){

		$("#status").html("");

		var jsonUrl = $.trim($("#textarea").val());
		var jqxhr = $.ajax({
					    url: jsonUrl,
					    dataType: "json",
					    beforeSend: function() {
					    				$("#submit").html(" Loading...");
					    				$("#submit").prepend("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>");
					    			}	
					})
					.done (function(data) { 
						$("#submit span").remove();
						$("#form").hide();
						$("#content").show();

						var array = $.makeArray(data);

						var list = "<ul class='data'>";

						$.each(array, function(i, item) {
							$.each(item.items, recursive);
						});	

					    function recursive(key, val){
					        
					        var value = val['items'];

					        if (value instanceof Object) {				        	
					            list += "<li class='folder'><a href='" + val.name + "'><span class='glyphicon glyphicon-folder-open'></span>" + val.name + "</a>";
								var ulClass = val.name.toLowerCase().replace(/\s/g, "");
								list += "<ul class='data " + ulClass + "'>";
								$.each(value, recursive)
								list += "</ul>";
					            list += "</li>";
					        } else{
					        	list += "<li class='pic' data-featherlight='http://static.panoramio.com/photos/large/" + val.name + "'><a href='" + val.name + "'><span class='glyphicon glyphicon-eye-open'></span>" + val.name + "</a></li>";
					        }

					    }

						list += "</ul>";

						$("#list").html(list);

						listenClick();

					})
					.fail (function(){
						$("#submit span").remove();
						$("#submit").html("Import");
						$("#status").show().html("<div class='alert alert-danger'>Error or not JSON format</div>")
					})
		;

	});


function listenClick(){

	// Clicking on li.folders
	$(document).on('click', '#list li.folder', function(e){
		e.preventDefault();
		var nextDir = $(this).find("a").attr("href"),
			ulToShow = $(this).find("ul.data").first().clone(),
			activeClass = nextDir.toLowerCase().replace(/\s/g, "");

		ulToShow.prepend("<li class='levelup'><a href='javascript:;'>...</a></li>");
		ulToShow.addClass(activeClass);

		$(this).parent().hide();
		$(this).parent().after(ulToShow);
		ulToShow.show();


		$(".breadcrumb li").removeClass("current");
		$(".breadcrumb").append("<li class='" + activeClass + "'><a href='javascript:;'>" + nextDir + "</a></li>");

	});	

	// click on li.levelup
	$(document).on("click", "li.levelup", function(){
		var currentUl = $(this).parent(),
			parrentUl = currentUl.prev("ul.data").first();
		parrentUl.show();
		parrentUl.find("ul.data").hide();
		currentUl.remove();
		$(".breadcrumb li").last().remove();
	})	

	// Clicking on breadcrumbs
	$(document).on('click', '.breadcrumb li a', function(e){
		e.preventDefault();

		if(!$(this).parent().hasClass("home")){
			var index = $(this).parent().attr("class");
			//$("ul.data").hide();
			$("ul.data." + index).show();
			$("ul.data." + index).find("ul.data").hide();
			$("ul.data." + index).nextAll($("ul.data")).remove();
			$(this).parent().nextAll().remove();
		} else {
			$(this).parent().nextAll().remove();
			var homeUl = $("#list ul.data").first();
			homeUl.show();
			homeUl.find("ul.data").hide();
			homeUl.nextAll($("ul.data")).remove();
		}

	});

	// check if img exist
	$(document).on("click", "li.pic", function(){
		img_url = $(this).data("featherlight"); 
		$("<img>", { src: img_url, error: function() { alert("Image broken or not exist!"); return false; }, load: function() { } });		
	})

}


})();